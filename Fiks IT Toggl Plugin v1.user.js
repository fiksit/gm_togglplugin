// ==UserScript==
// @name         Fiks IT Toggl Plugin - Description warning
// @namespace    https://fiksit.dk/
// @version      1.4
// @description  Notify user when toggle description length is above 50 characters.
// @author       Oliver Karstoft @ Fiks IT
// @match        https://track.toggl.com/timer
// @grant        GM_addStyle
// @run-at       document-idle
// @downloadURL  https://gitlab.com/fiksit/gm_togglplugin/raw/master/Fiks%20IT%20Toggl%20Plugin%20v1.user.js
// @updateURL    https://gitlab.com/fiksit/gm_togglplugin/raw/master/Fiks%20IT%20Toggl%20Plugin%20v1.user.js
// ==/UserScript==

(function() {

    // Change these variables to TRUE or FALSE to enable or disable features.
    // (MUST BE UPPERCASE!)
    const EnableCharacterCounter = 'TRUE'
    const EnableOldEntryHighlight = 'TRUE'
    const EnablePulseEffect = 'FALSE'
    const EnableAutoLengthCut = 'FALSE'

    // Collect toggl elements
    const inputClass = 'e1ln5t0v1'
    const inputElem = document.getElementsByClassName(inputClass)

    /***************************************
           DO NOT EDIT BELOW THIS LINE
    ***************************************/

    console.log('Fiks IT Toggl Plugin - Description warning is initialized')
    console.log('Version 1.3, published 01/10/19')

    // Create pulse class
    GM_addStyle (`
    @-webkit-keyframes pulse {
        0% {
            //-webkit-transform: scale(1, 1);
            opacity: 1;
        }
        50% {
            //-webkit-transform: scale(1.1, 1.1);
            opacity: 0.5;
        }
        100% {
            //-webkit-transform: scale(1, 1);
            opacity: 1;
        };
    }

    @keyframes pulse {
        0% {
            //transform: scale(1, 1);
            opacity: 1;
        }
        50% {
            //transform: scale(1.04, 1.04);
            opacity: 0.5;
        }
        100% {
            //transform: scale(1, 1);
            opacity: 1;
        };
    }

    .pulse-red-current {
        border: 2px solid red !important;
        color: red !important;
        -webkit-animation: pulse 0.5s linear infinite;
	    animation: pulse 0.7s linear infinite;
    }

    .pulse-red-list {
        border: 2px solid red !important;
        -webkit-animation: pulse 0.5s linear infinite;
	    animation: pulse 0.7s linear infinite;
    }`);

    // Add event listener
    document.body.addEventListener('input', function (event) {
        if (event.target.className.includes('e1ln5t0v1')) {
            const lengthCounterElem = document.getElementById('inputLengthField')

            // ##################
            // Length >50
            if (inputElem[0].value.length > 50) {
                if (EnablePulseEffect === 'TRUE') {
                    inputElem[0].classList.add('pulse-red-current')
                } else {
                    inputElem[0].style.border = '2px solid red'
                    inputElem[0].style.setProperty('color', 'red', 'important')
                }

                if (EnableAutoLengthCut === 'TRUE') {
                    inputElem[0].value = inputElem[0].value.substring(0,50)
                }

                if (EnableCharacterCounter === 'TRUE') {
                    lengthCounterElem.style.color = 'red'
                }
            } else {

                if (EnablePulseEffect === 'TRUE') {
                    inputElem[0].classList.remove('pulse-red-current')
                } else {
                    inputElem[0].style.border = 'none'
                    inputElem[0].style.color = ''
                }

                if (EnableCharacterCounter === 'TRUE') {
                    lengthCounterElem.style.color = 'darkgrey'
                }
            }

            // ###################
            // Length view
            if (EnableCharacterCounter === 'TRUE') {
                lengthCounterElem.innerHTML = inputElem[0].value.length + '/50'
            }
        }

        if (EnableCharacterCounter === 'TRUE') {
            const listElements = document.getElementsByClassName('e10vqxue0')
            for (let i = 0; i<listElements.length; i++) {
                // Update charCounterList
                if (listElements[i].parentNode.parentNode.parentNode.parentNode.nextSibling.classList.contains('inputListLengthField')) {
                    listElements[i].parentNode.parentNode.parentNode.parentNode.nextSibling.innerHTML = listElements[i].innerHTML.length + '/50'
                } else {
                    // Create counter
                    const charCounterList = document.createElement('div')
                    charCounterList.innerHTML = listElements[i].innerHTML.length + '/50'
                    charCounterList.style.padding = '10px'
                    charCounterList.style.color = 'darkgrey'
                    charCounterList.className = 'inputListLengthField'
                    listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.insertBefore(charCounterList, document.getElementsByClassName('e1t0hi2n0')[i])
                }
            }
        }

        if (EnableOldEntryHighlight === 'TRUE') {
            const listElements = document.getElementsByClassName('e10vqxue0')
            for (let i = 0; i<listElements.length; i++) {
                if (listElements[i].innerHTML.length > 50) {
                    if (EnablePulseEffect === 'TRUE') {
                        listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.classList.add('pulse-red-list')
                    } else {
                        listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.style.border = '2px solid red'
                        listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.style.setProperty('color', 'red', 'important')
                    }
                } else {
                    if (EnablePulseEffect === 'TRUE') {
                        listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.classList.remove('pulse-red-list')
                    } else {
                        listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.style.border = 'none'
                        listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.style.setProperty('color', '')
                    }
                }

                
            }
        }
    }, false)

    // Add length counter
    if (EnableCharacterCounter === 'TRUE') {
        setTimeout(function () {
            // Initialize charCounter in currentToggl
            const charCounterCurrent = document.createElement('div')
            charCounterCurrent.innerHTML = '0/50'
            charCounterCurrent.style.padding = '10px'
            charCounterCurrent.style.color = 'darkgrey'
            charCounterCurrent.id = 'inputLengthField'
            document.getElementsByClassName('TimerFormProject__container')[0].parentNode.insertBefore(charCounterCurrent, document.getElementsByClassName('TimerFormProject__container')[0])

            // Create list counter
            const listElements = document.getElementsByClassName('e10vqxue0')
            for (let i = 0; i<listElements.length; i++) {
                const charCounterList = document.createElement('div')
                charCounterList.innerHTML = listElements[i].innerHTML.length + '/50'
                charCounterList.style.padding = '10px'
                charCounterList.style.color = 'darkgrey'
                charCounterList.className = 'inputListLengthField'
                listElements[i].parentNode.parentNode.parentNode.parentNode.parentNode.insertBefore(charCounterList, document.getElementsByClassName('e1t0hi2n0')[i])
            }
        }, 5000);
    }

})();
